/**
 * 根据表生成POJO，Mapper，Sql文件，数据库文档
 * <ul>
 *     <li>@{link EntitySourceBuilder} 生成pojo代码</li>
 *     <li>@{link MapperSourceBuilder} 生成mapper代码</li>
 *     <li>@{link MDDocBuilder},生成markdown的文档</li>
 *     <li>@{link MDSourceBuilder},生成sql文件</li>
 *     <li>@{link MDSourceBuilder},生成sql文件</li>
 * </ul>
 *
 *
 */
package org.beetl.sql.gen.simple;