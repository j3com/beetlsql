package org.beetl.sql.saga.demo.mapper;

import org.beetl.sql.saga.common.SagaMapper;
import org.beetl.sql.saga.demo.entity.OrderEntity;
import org.beetl.sql.saga.demo.entity.UserEntity;

public interface UserMapper extends SagaMapper<UserEntity> {
}
